package it.unimi.di.sweng.lab06;

import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.Timeout;
import static org.junit.Assert.*;
import it.unimi.di.sweng.lab06.GiraParole;
import java.util.HashSet;

public class ExampleTest {

    @Rule
    public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
   
    @Test
     public void testParolaIgnorata() throws Exception {
        ParoleDaIgnorare paroleDaIgnorare = ParoleDaIgnorare.ottieniParoleDaIgnorare();
       
        paroleDaIgnorare.aggiungiParolaDaIgnorare("e");
        assertTrue(paroleDaIgnorare.parolaIgnorata("e"));
        paroleDaIgnorare.rimuoviParolaDaIgnorare("e");
       
        paroleDaIgnorare.aggiungiParolaDaIgnorare("non");
        assertTrue(paroleDaIgnorare.parolaIgnorata("non"));
        paroleDaIgnorare.rimuoviParolaDaIgnorare("non");
       
        paroleDaIgnorare.aggiungiParolaDaIgnorare(",");
        assertTrue(paroleDaIgnorare.parolaIgnorata(","));
        paroleDaIgnorare.rimuoviParolaDaIgnorare(",");
       
        paroleDaIgnorare.aggiungiParolaDaIgnorare("!");
        assertTrue(paroleDaIgnorare.parolaIgnorata("!"));
        paroleDaIgnorare.rimuoviParolaDaIgnorare("!");
       
        paroleDaIgnorare.aggiungiParolaDaIgnorare("?");
        assertTrue(paroleDaIgnorare.parolaIgnorata("?"));
        paroleDaIgnorare.rimuoviParolaDaIgnorare("?");
       
        paroleDaIgnorare.aggiungiParolaDaIgnorare(";");
        assertTrue(paroleDaIgnorare.parolaIgnorata(";"));
        paroleDaIgnorare.rimuoviParolaDaIgnorare(";");
       
    }
    @Test
    public void testGiraParole() throws Exception {
        GiraParole giroDiParole = new GiraParole("testa questo giro di parole");
            String[] spostamenti = giroDiParole.ottieniGiraParole();
            HashSet<String> setDiSpostamenti = new HashSet<String>();
            for (String str : spostamenti) {
                setDiSpostamenti.add(str);
            }
            assertTrue(setDiSpostamenti.size() == 5);
            assertTrue(setDiSpostamenti.contains("Testa Questo Giro Di Parole"));
            assertTrue(setDiSpostamenti.contains("Questo Giro Di Parole Testa"));
            assertTrue(setDiSpostamenti.contains("Giro Di Parole Testa Questo"));
            assertTrue(setDiSpostamenti.contains("Di Parole Testa Questo Giro"));
            assertTrue(setDiSpostamenti.contains("Parole Testa Questo Giro Di"));
    }
    
    @Test
    public void testPerOttenerePaoleOrdinate() throws Exception {

        OrdineAlfabetico ordineAlf = new OrdineAlfabetico();

        String[] linee = {"questo è un", "semplice test", "di prova", "per ordinare", "le linee", "in ordine", "alfabetico"};
        ordineAlf.aggiungiLinee(linee);
        String[] output = ordineAlf.ottieniLineeOrdinate();
        assertTrue(output.length == 7);
        assertEquals("alfabetico", output[0]);
        assertEquals("di prova", output[1]);
        assertEquals("in ordine", output[2]);
        assertEquals("le linee", output[3]);
        assertEquals("per ordinare", output[4]);
        assertEquals("questo è un", output[5]);
        assertEquals("semplice test", output[6]);
    }
    
}