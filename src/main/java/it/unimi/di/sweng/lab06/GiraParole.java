package it.unimi.di.sweng.lab06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class GiraParole {

    public static String delim = " ";
    private String linea;
    private ParoleDaIgnorare paroleDaIgnorare;

   
    public GiraParole(String linea) {
        assert(linea != null);
        this.linea = linea.toLowerCase();
        this.paroleDaIgnorare = ParoleDaIgnorare.ottieniParoleDaIgnorare();
    }

    public String[] ottieniGiraParole() {
        String[] parole = this.linea.split(delim);
        String[] spostamento = new String[parole.length];
        spostamento[0] = this.linea;

        for (int i=1;i<parole.length;i++) {
            spostamento[i] = this.ottieniSpostamentoLinea(i, parole);
        }

        String[] spostamentoFiltrato = ottieniSpostamentoSenzaIgnorarePrimaParola(spostamento);
        for (int i=0;i<spostamentoFiltrato.length;i++) {
            spostamentoFiltrato[i] = accumulareParoleNonIgnorateInShift(spostamentoFiltrato[i]);
        }

        return spostamentoFiltrato;
    }

    private String ottieniSpostamentoLinea(int index, String[] parole) {
        StringBuilder builder = new StringBuilder();

        for (int i=index;i<parole.length;i++) {
            builder.append(parole[i]);
            builder.append(delim);
        }
        for (int i=0;i<index;i++) {
            builder.append(parole[i]);
            builder.append(delim);
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }

        return builder.toString();
    }

    private String[] ottieniSpostamentoSenzaIgnorarePrimaParola(String[] spostamento) {
        List<String> listaShiftata = new ArrayList<String>(Arrays.asList(spostamento));

        Iterator<String> iter = listaShiftata.iterator();
        while (iter.hasNext()) {
            if (spostamentoIniziaConParolaIgnorata(iter.next())) {
                iter.remove();
            }
        }

        return listaShiftata.toArray(new String[listaShiftata.size()]);
    }

    private boolean spostamentoIniziaConParolaIgnorata(String linea) {
        return this.paroleDaIgnorare.parolaIgnorata(linea.split(delim)[0]);
    }

    private String accumulareParoleNonIgnorateInShift(String shift) {
        String[] words = shift.split(delim);
        StringBuilder builder = new StringBuilder();

        for (String str : words) {
            if (this.paroleDaIgnorare.parolaIgnorata(str)) {
                builder.append(str);
            } else if (str.trim().isEmpty()) {
                continue;
            } else {
                builder.append(Character.toUpperCase(str.charAt(0))).append(str.substring(1));
            }
            builder.append(delim);
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }

        return builder.toString();
    }
}