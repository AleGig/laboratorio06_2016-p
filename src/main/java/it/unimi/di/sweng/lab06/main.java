package it.unimi.di.sweng.lab06;

import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class main {
    public static void main(String[] args){
       
         Scanner scanner = new Scanner(System.in);

            System.out.println("Inserisci corpus (termina l'input inserendo una riga vuota) ");

            List<String> inputs = new ArrayList<String>();
            String input = scanner.nextLine();
            while (!input.isEmpty()) {
                inputs.add(input);
                input = scanner.nextLine();
            }

        System.out.println("Immetti parole da ignorare (termina l'input con una riga vuota) ");
        String inputParolaDaIgnorare = scanner.nextLine();
        ParoleDaIgnorare paroleDaIgnorare = ParoleDaIgnorare.ottieniParoleDaIgnorare();

        while (!inputParolaDaIgnorare.isEmpty()) {
            paroleDaIgnorare.aggiungiParolaDaIgnorare(inputParolaDaIgnorare);
            inputParolaDaIgnorare = scanner.nextLine();
        }

        OrdineAlfabetico ordine = new OrdineAlfabetico();
            for (String str : inputs) {
                    GiraParole shifter = new GiraParole(str);
                    ordine.aggiungiLinee(shifter.ottieniGiraParole());
            }

        String[] risultato = ordine.ottieniLineeOrdinate();
       
        StringBuilder builder = new StringBuilder();
        String separator = System.lineSeparator();
            for (String str : risultato) {
                builder.append(str).append(separator);
            }
        System.out.print(builder.toString());
        }
    }    