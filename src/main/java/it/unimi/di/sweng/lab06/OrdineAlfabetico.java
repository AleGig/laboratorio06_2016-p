package it.unimi.di.sweng.lab06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OrdineAlfabetico {

   private List<String> linee;
     
   public OrdineAlfabetico() {
       this.linee = new ArrayList<String>();
    }
 
   public void aggiungiLinee(String[] linee) {
        for (String str : linee) {
            this.linee.add(str);
        }
    }

    public String[] ottieniLineeOrdinate() {
        Collections.sort(this.linee);
        return this.linee.toArray(new String[this.linee.size()]);
    }
}