package it.unimi.di.sweng.lab06;

import java.util.HashSet;

public class ParoleDaIgnorare {
   
    private HashSet<String> paroleDaIgnorare;
    private static ParoleDaIgnorare istanza;
    private ParoleDaIgnorare() {
        this.paroleDaIgnorare = new HashSet<String>();
    }
   
    public static ParoleDaIgnorare ottieniParoleDaIgnorare() {
        if (istanza == null) {
            istanza = new ParoleDaIgnorare();
        }

        return istanza;
   
    }

    public boolean parolaIgnorata(String parola) {
        assert(parola != null);
        return this.paroleDaIgnorare.contains(parola);
    }

    public void aggiungiParolaDaIgnorare(String parola) {
        assert(parola != null);
        this.paroleDaIgnorare.add(parola);
       
    }

    public void rimuoviParolaDaIgnorare(String parola) {
        assert(parola != null);
        this.paroleDaIgnorare.remove(parola);
       
    }


}